// clang-format off
include <utils/utils.scad>;
// clang-format on

// 📏 total length of the OUTER shell
length = 30;
// 📏 How far the INNER shell should stick out of the OUTER shell (excl chamfer)
clearance_length = 10;
// 🦷 Number of teeth/wobbles per circumference
teeth = 3;
// Relative Width of the tips.    thin ↔ thick
teeth_width_relative = 0.5;                            // [0:0.01:1]
teeth_exponent = tan((1 - teeth_width_relative) * 90); // [0:0.1:10]
twist_per_mm = 5;                                      // [0:0.1:10]
twist_clockwise = false;
twist = (twist_clockwise ? -1 : 1) * length * twist_per_mm;
chamfer_height = 3;
text = ""; // text to display on lid and bottom
// text to display on lid and bottom
text_size = 0.9; // [0.1:0.01:1]
// see Help -> Font List for possible strings
text_font = "";

// inner diameter of the OUTER shell (I know, it's stupid...)
inner_diameter = 10;
// outer diameter of the OUTER shell
outer_diameter = 35;
inner_radius = inner_diameter / 2;
outer_radius = outer_diameter / 2;
// the perimeter thickness you will print with in vase mode
wall_thickness = 1; // [0.1:0.01:3]
// 📐 Clearance between inner and outer shell. At least your printer's
/* [precision] */
epsilon = 0.1;
wall_clearance = 0.1;
wall_clearance_horizontal =
  max(wall_clearance, wall_clearance / sin(atan(1 / twist_per_mm)) / 2);

echo(wall_clearance = wall_clearance,
     wall_clearance_horizontal = wall_clearance_horizontal);

$fs = $preview ? 1 : 0.5;
$fa = $preview ? 2 : 0.5;

function sin_between(x, lower = 0, upper = 1, exponent = 1) =
  lower + (upper - lower) * pow2((sin(x) + 1) / 2, exponent);

function wobbly_circle(radius = [ 1, 1 ],
                       n = 5,
                       exponent = 1,
                       $fn = 360 / $fa) =
  multiply(
    // unit circle
    [for (i = [0:$fn - 1])[cos(i / $fn * 360), sin(i / $fn * 360)]],
    // sine wave oscillating between inner and outer radius
    [for (i = [0:$fn - 1]) sin_between(i / $fn * 360 * n,
                                       lower = radius[0],
                                       upper = radius[1],
                                       exponent = exponent)]);

module
screw_container(radius = [ inner_radius, outer_radius ],
                length = length,
                teeth = teeth,
                teeth_exponent = teeth_exponent,
                twist = twist,
                slices = length / $fs,
                scale = 1,
                chamfer_height = 0,
                text = "")
{
  difference()
  {
    if (chamfer_height <= 0) {
      outline =
        wobbly_circle(radius = radius, n = teeth, exponent = teeth_exponent);
      linear_extrude(length, twist = twist, slices = slices, scale = scale)
        polygon(outline);
    } else {
      // recursive definition
      translate([ 0, 0, chamfer_height ]) rotate([ 0, 180, 0 ])
        screw_container(radius = radius,
                        length = chamfer_height,
                        teeth = teeth,
                        twist = -chamfer_height / length * twist,
                        slices = slices,
                        scale = 1 - chamfer_height / outer_radius);
      translate([ 0, 0, chamfer_height ])
        rotate([ 0, 0, teeth % 2 == 0 ? 360 / teeth / 2 : 0 ])
          screw_container(radius = radius,
                          length = length - chamfer_height,
                          teeth = teeth,
                          slices = slices,
                          twist = -(twist - chamfer_height / length * twist));
    }
    if (len(text) > 0) {
      resize(
        [
          text_size *
            (inner_diameter - 4 * wall_thickness - 2 * chamfer_height),
          0
        ],
        auto = true) mirror([ 1, 0, 0 ]) translate([ 0, 0, -epsilon ])
        linear_extrude(0.5 * wall_thickness + epsilon)
          text(text, halign = "center", valign = "center", font = text_font);
    }
  }
}

screw_container(length = length,
                chamfer_height = chamfer_height,
                twist = twist,
                text = text);
translate([ outer_diameter * 1.5, 0, 0 ])
  screw_container(radius =
                    [
                      inner_radius - wall_thickness - wall_clearance_horizontal,
                      outer_radius - wall_thickness -
                      wall_clearance_horizontal
                    ],
                  length = length + clearance_length,
                  twist = (length + clearance_length) / length * twist,
                  chamfer_height = chamfer_height,
                  text = text);
