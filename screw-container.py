import cadquery as cq
import numpy as np

inner_radius = 4.5
outer_radius = 5
n_teeth = 5

# curve = cq.Workplane("XY").parametricCurve(
#     lambda t: tuple(
#         np.array([np.cos(t), np.sin(t), 0])
#         * ((outer_radius - inner_radius) * (np.sin(n_teeth * t) + 1) / 2 + inner_radius)
#     ),
#     N=60,
#     start=0,
#     stop=2 * np.pi,
# )

import cadquery as cq
from math import sin, cos, pi, floor

# define the generating function
def hypocycloid(t, r1, r2):
    return (
        (r1 - r2) * cos(t) + r2 * cos(r1 / r2 * t - t),
        (r1 - r2) * sin(t) + r2 * sin(-(r1 / r2 * t - t)),
    )


def epicycloid(t, r1, r2):
    return (
        (r1 + r2) * cos(t) - r2 * cos(r1 / r2 * t + t),
        (r1 + r2) * sin(t) - r2 * sin(r1 / r2 * t + t),
    )


def gear(t, r1=4, r2=1):
    if (-1) ** (1 + floor(t / 2 / pi * (r1 / r2))) < 0:
        return epicycloid(t, r1, r2)
    else:
        return hypocycloid(t, r1, r2)


# create the gear profile and extrude it
result = (
    cq.Workplane("XY")
    .parametricCurve(lambda t: gear(t * 2 * pi, 6, 1))
    .twistExtrude(15, 90)
    .faces(">Z")
    .fillet(1)
    # .workplane()
    # .circle(2)
    # .cutThruAll()
)

# lower_half = curve.twistExtrude(1, 1)
